/*Q2:Find maximum sum path involving elements of given arrays
Given two sorted arrays of integers, find a maximum sum path involving elements of both arrays whose sum is maximum. 
We can start from either array, but we can switch between arrays only through its common elements.

For example,

Input: X = { 3, 6, 7, 8, 10, 12, 15, 18, 100 }
Y = { 1, 2, 3, 5, 7, 9, 10, 11, 15, 16, 18, 25, 50 }  
The maximum sum path is: 1 —> 2 —> 3 —> 6 —> 7 —> 9 —> 10 —> 12 —> 15 —> 16 —> 18 —> 100 
The maximum sum is 199*/
import java.io.*;
class MaximumSumPath {
    int findMaxSumPath(int[] X, int[] Y) {
        int sumX = 0, sumY = 0, maxSum = 0;
        int i = 0, j = 0;

        while (i < X.length && j < Y.length) {
            if (X[i] < Y[j]) {
                sumX += X[i++];
            } else if (X[i] > Y[j]) {
                sumY += Y[j++];
            } else { 
                maxSum += Math.max(sumX, sumY) + X[i];
                sumX = 0;
                sumY = 0;
                i++;
                j++;
            }
        }

      
        while (i < X.length) {
            sumX += X[i++];
        }
        while (j < Y.length) {
            sumY += Y[j++];
        }

        
        maxSum += Math.max(sumX, sumY);

        return maxSum;
    }
    public static void main(String[] args)throws IOException{
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
                int Xsize;
                System.out.println("Enter Xarray size");
                do{
                        Xsize=Integer.parseInt(br.readLine());
                        if(Xsize<=0){
                                System.out.println("Re-Enter Xarray size");
                        }
                }while(Xsize<=0);

                int X[] =new int[Xsize];

                System.out.println("Enter Xarray elements");
                for(int i=0;i<X.length;i++){
                        X[i]=Integer.parseInt(br.readLine());
                }
                int Ysize;
                System.out.println("Enter Yarray size");
                do{
                        Ysize=Integer.parseInt(br.readLine());
                        if(Ysize<=0){
                                System.out.println("Re-Enter Yarray size");
                        }
                }while(Ysize<=0);

                int Y[] =new int[Ysize];

                System.out.println("Enter Yarray elements");
                for(int i=0;i<Y.length;i++){
                        Y[i]=Integer.parseInt(br.readLine());
                }
                MaximumSumPath obj=new MaximumSumPath();
                int ans= obj.findMaxSumPath(X,Y);
		System.out.println("The maximum sum path is: " + ans);
    }
}
/*Enter Xarray size
9
Enter Xarray elements
3
6
7
8
10
12
15
18
100
Enter Yarray size
13
Enter Yarray elements
1
2
3
5
7
9
10
11
15
16
18
25
50
The maximum sum path is: 199*/

