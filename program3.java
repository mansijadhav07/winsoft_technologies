//Q3:Write a Java Program to count the number of words in a string using HashMap.
import java.util.HashMap;
import java.util.Map;
import java.io.*;
class WordCount {

    static void countWords(String str) {
        Map<String, Integer> wordCountMap = new HashMap<>();
        String[] words = str.split("\\s+"); 

        for (String word : words) {
            word = word.toLowerCase();
            if (wordCountMap.containsKey(word)) {
                wordCountMap.put(word, wordCountMap.get(word) + 1);
            } else {
                wordCountMap.put(word, 1);
            }
        }

        
        int totalWords = 0;
        for (Map.Entry<String, Integer> entry : wordCountMap.entrySet()) {
            totalWords += entry.getValue();
        }
        System.out.println("Total words: " + totalWords);
    }

    public static void main(String[] args)throws IOException{
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter a string: ");
        String str = br.readLine();
        countWords(str);
    }
}
/*Enter a string:
I am Mansi Praksh Jadhav
Total words: 5*/

