/*Q1: Merge two arrays by satisfying given constraints
Given two sorted arrays X[] and Y[] of size m and n each where m >= n and X[] has exactly n vacant cells,
 merge elements of Y[] in their correct position in array X[], i.e., merge (X, Y) by keeping the sorted order.

For example,

Input: X[] = { 0, 2, 0, 3, 0, 5, 6, 0, 0 }
Y[] = { 1, 8, 9, 10, 15 } The vacant cells in X[] is represented by 0 
Output: X[] = { 1, 2, 3, 5, 6, 8, 9, 10, 15 }*/
import java.io.*;
class Array{
	void merge(int[] X,int[] Y){
	int m = X.length;
        int n = Y.length;
        int index = m - 1;

        for (int i = m - 1; i >= 0; i--) {
            if (X[i] != 0) {
                X[index] = X[i];
                index--;
            }
        }

        int i = index + 1;
        int j = 0;
        int k = 0;
        while (i < m && j < n) {
            if (X[i] < Y[j]) {
                X[k++] = X[i++];
            } else {
                X[k++] = Y[j++];
            }
        }

        while (j < n) {
            X[k++] = Y[j++];
        }
    }
    public static void main(String[] args)throws IOException{
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
                int Xsize;
                System.out.println("Enter Xarray size");
                do{
                        Xsize=Integer.parseInt(br.readLine());
                        if(Xsize<=0){
                                System.out.println("Re-Enter Xarray size");
                        }
                }while(Xsize<=0);

                int X[] =new int[Xsize];

                System.out.println("Enter Xarray elements");
                for(int i=0;i<X.length;i++){
                        X[i]=Integer.parseInt(br.readLine());
                }
		int Ysize;
                System.out.println("Enter Yarray size");
                do{
                        Ysize=Integer.parseInt(br.readLine());
                        if(Ysize<=0){
                                System.out.println("Re-Enter Yarray size");
                        }
                }while(Ysize<=0);

                int Y[] =new int[Ysize];

                System.out.println("Enter Yarray elements");
                for(int i=0;i<Y.length;i++){
                        Y[i]=Integer.parseInt(br.readLine());
                }
		Array obj=new Array();
		obj.merge(X,Y);
		System.out.println("Output Array");
		for(int i=0;i<X.length;i++){
                        System.out.print(X[i]+" ");
                }
		System.out.println();
		
	}
 }
/*
Enter Xarray size
9
Enter Xarray elements
0
2
0
3
0
5
6
0
0
Enter Yarray size
5
Enter Yarray elements
1
8
9
10
15
Output Array
1 2 3 5 6 8 9 10 15*/

		 
